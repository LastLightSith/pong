extends TileMap

signal lost

func check_tile(pos):
	var tilepos = world_to_map(pos)
	var tile = get_cellv(tilepos)
	match tile:
		2:
			emit_signal('lost')
			return true
		1:
			set_cellv(tilepos,-1)
			global.score += 1
			get_node("../score").text = "Score:" + str( global.score)
			
			if(global.score == get_node("..").WinScore):
				print("should Win")
				global.completedLevel = int(get_tree().current_scene.name )
				if(global.level <= global.completedLevel+1):
					global.level += 1

				get_tree().change_scene("res://scenes/WinScene.tscn")
			
			return true
		-1:
			return false


func _on_ball_collided(pos):
	var chancepos = [ Vector2(pos.x-2,pos.y-2),Vector2(pos.x-2,pos.y+2),Vector2(pos.x+2,pos.y+2),Vector2(pos.x+2,pos.y-2) ]
	for vec in chancepos:
		var con = check_tile(vec)
		if con:
			return
	#print("something bad happend")