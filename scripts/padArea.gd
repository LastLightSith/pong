extends Area2D

onready var pos = get_node("..").position
onready var move = false
func _ready():
	set_process_input(true)


func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton \
	and event.button_index == BUTTON_LEFT :	
		move = !move
		
func _process(delta):
	if(move):
		var tmp = pos
		pos.x = get_viewport().get_mouse_position().x
		get_node("..").move_and_collide(pos-tmp)

func _on_padArea_mouse_exited():
	move = false
