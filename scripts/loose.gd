extends Node2D

func _ready():
	get_node('brestart').connect('pressed',self,'_reset_game')
	get_node('bquit').connect('pressed',self,'_quit_game')

	pass

func _reset_game():
	global.score = 0
	print('res://scenes/levels/0'+  str(global.level)+ '.tscn')
	get_tree().change_scene('res://scenes/levels/0'+ str(global.level)+ '.tscn')
	queue_free()

func _quit_game():
	global.save()
	get_tree().quit()

func _on_bhome_pressed():
	get_tree().change_scene('res://scenes/home.tscn')
