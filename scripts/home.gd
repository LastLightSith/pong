extends Node2D

func _ready():
	get_node('bstart').connect('pressed',self,'start_game')
	get_node('bquit').connect('pressed',self,'quit_game')
	pass

func start_game():
	get_tree().change_scene('res://scenes/levels/01.tscn')

func quit_game():
	global.save()
	get_tree().quit()

func _on_blevels_pressed():
		get_tree().change_scene('res://scenes/Levels.tscn')