extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	var buttons = get_children()
	
	for i in buttons:
		if(int(i.text)-1 > global.completedLevel):
			i.disabled = true
		i.connect("pressed",self,"ButtonClicked",[i.text])

func ButtonClicked(num):
	var level = "res://scenes/levels/" + num + ".tscn"
	get_tree().change_scene(level)