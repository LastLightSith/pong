extends KinematicBody2D

var vel = Vector2(0,0)
var rCount = 0
const SPEED = 1300


func _ready():
	pass

func getinput():
	if(Input.is_action_pressed('ui_left') ):
		vel.x -= 1
	if(Input.is_action_pressed('ui_right') ):
		vel.x += 1
	var maxDeg = rad2deg(rotation)
	if(maxDeg  > -20 ):
		if(Input.is_action_pressed('ui_up') ):
			rCount -= 1
			print(rCount)
	if(maxDeg < 20):
		if(Input.is_action_pressed('ui_down') ):
			rCount += 1
			print(rCount)

func _physics_process(delta):
	vel.x = 0
	getinput()
	move_and_collide(vel *delta * SPEED)
	rotation = self.rotation + deg2rad(rCount*delta*SPEED/5)
	rCount = 0

